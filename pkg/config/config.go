package config

import (
	"fmt"

	"github.com/gookit/config/v2"
	"github.com/gookit/config/v2/yaml"
)

type Config struct {
	User   string `mapstructure:"user"`
	Pat    string `mapstructure:"pat"`
	Server string `mapstructure:"server"`
}

func Load() Config {
	config.WithOptions(config.ParseEnv)
	config.AddDriver(yaml.Driver)
	err := config.LoadFiles(".gitlab-standup-buddy.yml")
	if err != nil {
		panic(err)
	}

	cnf := Config{}
	err = config.BindStruct("", &cnf)
	if err != nil {
		panic(err)
	}

	fmt.Println("user: " + cnf.User)
	fmt.Println("server: " + cnf.Server)

	return cnf
}
