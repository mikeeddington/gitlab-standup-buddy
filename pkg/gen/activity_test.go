package gen

import (
	"testing"
)

func Test_getUserActivity_processActivityEntries(t *testing.T) {
	entries := getUserActivity()
	if len(*entries) == 0 {
		t.Errorf("Result was incorrect, got: %d, want: %s.", len(*entries), ">0 items")
	}

	t.Log("Unknown:", Unknown, "MrAcceptedMr", MrAcceptedMr)

	processActivityEntries(entries)

	for _, entry := range *entries {
		if entry.Type == Unknown {
			t.Error("Entry of type Unknown", entry.Type, "found. Title:", entry.Title)
		}
	}
}
