package gen

import (
	"github.com/rs/zerolog/log"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/mikeeddington/gitlab-standup-buddy/pkg/config"
)

func Generate() string {
	cnf := config.Load()

	activities := GetUserActivity(cnf.User)

	gl, err := gitlab.NewClient(
		cnf.Pat,
		gitlab.WithBaseURL(cnf.Server+"/api/v4"))
	if err != nil {
		log.Printf("Failed to create Gitlab client: %v", err)
		panic(err)
	}

	for _, activity := range *activities {

		if activity.Type == BranchPushTo || activity.Type == BranchPushNew {
			// Worked on an MR
		}
		if activity.Type == MrOpened {
			// Worked on an MR
		}
		if activity.Type == MrApproved || activity.Type == MrCommentedOn {
			// Reviewed an MR (maybe)
		}
		if activity.Type == IssueOpened {
			// Created and issue
		}
		if activity.Type == IssueClosed {
			// Completed issue (check tags)
		}
	}

	return ""
}
