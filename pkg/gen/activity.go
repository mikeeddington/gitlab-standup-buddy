package gen

import (
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"
)

type ActivityType int

const (
	Unknown ActivityType = iota
	BranchDeleted
	BranchPushNew
	BranchPushNewProject
	BranchPushTo
	BranchPushToProject
	IssueCommentedOn
	IssueOpened
	IssueClosed
	MrApproved
	MrOpened
	MrCommentedOn
	MrAcceptedMr // Merged
	MrClosed
	ProjectCreated
	TagDeleted
	TagPushedNew
)

type ActivityFeed struct {
	XMLName xml.Name        `xml:"feed"`
	Updated time.Time       `xml:"updated"`
	Entries []ActivityEntry `xml:"entry"`
}

type ActivityEntry struct {
	XMLName xml.Name     `xml:"entry"`
	Id      string       `xml:"id"`
	Link    string       `xml:"link"`
	Title   string       `xml:"title"`
	Updated time.Time    `xml:"updated"`
	Type    ActivityType `xml:"-"`
}

func GetUserActivity(username string) *[]ActivityEntry {

	entries := getUserActivity(username)
	processActivityEntries(entries)

	return entries
}

func IsUserActivityFromToday(entry ActivityEntry) bool {
	timeToday := time.Now()

	return entry.Updated.Year() == timeToday.Year() &&
		entry.Updated.YearDay() == timeToday.YearDay()
}

func IsUserActivityFromYesterday(entry ActivityEntry) bool {
	timeYesterday := time.Now().AddDate(0, 0, -1)

	return entry.Updated.Year() == timeYesterday.Year() &&
		entry.Updated.YearDay() == timeYesterday.YearDay()
}

func getUserActivity(username string) *[]ActivityEntry {

	response, err := http.Get("https://gitlab.com/" + username + ".atom")
	if err != nil {
		fmt.Print(err.Error())
		panic(err)
	}

	responseData, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}

	var responseObject ActivityFeed
	err = xml.Unmarshal(responseData, &responseObject)
	if err != nil {
		panic(err)
	}

	return &responseObject.Entries
}

func processActivityEntries(entries *[]ActivityEntry) {

	for index, entry := range *entries {

		// Branches

		if strings.Contains(entry.Title, "deleted branch") {
			(*entries)[index].Type = BranchDeleted
		} else if strings.Contains(entry.Title, "pushed new branch") {
			(*entries)[index].Type = BranchPushNew
		} else if strings.Contains(entry.Title, "pushed new project branch") {
			(*entries)[index].Type = BranchPushNewProject
		} else if strings.Contains(entry.Title, "pushed to project branch") {
			(*entries)[index].Type = BranchPushToProject
		} else if strings.Contains(entry.Title, "pushed to branch") {
			(*entries)[index].Type = BranchPushTo

			// Issues

		} else if strings.Contains(entry.Title, "commented on issue") {
			(*entries)[index].Type = IssueCommentedOn
		} else if strings.Contains(entry.Title, "opened issue") {
			(*entries)[index].Type = IssueOpened
		} else if strings.Contains(entry.Title, "closed issue") {
			(*entries)[index].Type = IssueClosed

			// Merge Requests

		} else if strings.Contains(entry.Title, "approved merge request") {
			(*entries)[index].Type = MrApproved
		} else if strings.Contains(entry.Title, "opened merge request") {
			(*entries)[index].Type = MrOpened
		} else if strings.Contains(entry.Title, "commented on merge request") {
			(*entries)[index].Type = MrCommentedOn
		} else if strings.Contains(entry.Title, "accepted merge request") {
			(*entries)[index].Type = MrAcceptedMr
		} else if strings.Contains(entry.Title, "closed merge request") {
			(*entries)[index].Type = MrClosed

			// Projects

		} else if strings.Contains(entry.Title, "created project") {
			(*entries)[index].Type = ProjectCreated

			// Tags

		} else if strings.Contains(entry.Title, "deleted tag") {
			(*entries)[index].Type = TagDeleted
		} else if strings.Contains(entry.Title, "pushed new tag") {
			(*entries)[index].Type = TagPushedNew

		} else {
			(*entries)[index].Type = Unknown
			fmt.Println("Unknown activity title type:", entry.Title)
		}
	}
}
