package main

import "gitlab.com/mikeeddington/gitlab-standup-buddy/cmd"

func main() {
	cmd.Execute()
}
